import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Link, navigate } from "@reach/router";
import axios from 'axios';

import thanks from "../../images/badges/thanks.svg";
import diversity from '../../images/badges/diversity.svg';
import inspiring from '../../images/badges/inspiring.svg';
import teamPlayer from '../../images/badges/teamPlayer.svg';
import warm from '../../images/badges/warm.svg';
import supportive from '../../images/badges/supportive.svg';
import perspective from '../../images/badges/perspective.svg';
import beyond from '../../images/badges/beyond.svg';
require('dotenv').config();

const FormData = require('form-data');

function TeamMember(props) {
    const [user, setUser] = useState(null);
    const [userName, setUserName] = useState(null);
    const [team, setTeam] = useState(null);
    const [teammate, setTeammate] = useState(null);
    const [breadType, setBreadType] = useState(null);
    const [fname, setFname] = useState(null);

    /**
     * Get TeamID and Teammate props from Team page
     */
    useEffect(() => {
        const us = localStorage.getItem('user-email');
        setUser(us);
        sendGetRequest(us);

        const team = props.location.state.team;
        setTeam(team);
        const teammate = props.location.state.teammate;
        setTeammate(teammate);
        const splitName = teammate.credentials.name.split(' ');
        setFname(splitName[0]);

        const got = props.location.state.giveOrTeam;
        setBreadType(got);
    }, []);

    /**
 * get user data
 * @param {string} userEmail 
 * user email used as a reference to get data
 */
    const sendGetRequest = async (userEmail) => {
        try {
            const resp = await axios.get(`http://${process.env.SERVER_IP}/api/users/${userEmail}`, {});
            const userData = resp.data;
            //Await until we get the response
            if (resp.data.length === 1) {
                setUserName(userData[0].credentials.name);
            }
        } catch (err) {
            // Handle Error Here
            console.error(err);
        }
    };

    /**
 * sends appreciation to receiver
 * @param {*} sender 
 * email address of logged-in user
 * @param {*} receiver 
 * email address of receiver
 */
    const appreciate = (event) => {
        event.preventDefault();
        successMessage();

        let sender = userName;
        let receiver = teammate.credentials.email;
        let badge = event.target.badge.value;
        let comment = event.target.comment.value;

        if (comment === '') {
            let u = 'You ';
            switch (badge) {
                case 'thanks':
                    comment = 'Thanks for your work!';
                    break;
                case 'diversity':
                    comment = u + 'are a diversity champion.';
                    break;
                case 'beyond':
                    comment = u + 'went bbove and beyond.';
                    break;
                case 'warm':
                    comment = u + 'are warm and welcoming.';
                    break;
                case 'inspiring':
                    comment = u + 'inspire positivity.';
                    break;
                case 'teamPlayer':
                    comment = u + 'are a great team player.';
                    break;
                case 'supportive':
                    comment = u + 'are super supportive.';
                    break;
                case 'perspective':
                    comment = u + 'have a valuable perspective.';
                    break;
                default:
                    comment = u + 'are great!';
            }
        }

        axios.post('http://'+process.env.SERVER_IP+'/api/appr', {

            sender: { data: sender },
            badge: { data: badge },
            receiver: { data: receiver },
            comment: { data: comment },
        })
            .then((response) => {
            })
            .catch((error) => {
                console.log('error: ' + error);
            });

    };

    /**
     * Show and hide success message and navigate back to GiveAppreciation 
     */
    const successMessage = () => {
        document.getElementById("successMsg").style.visibility = "visible";
        setTimeout(() => {
            document.getElementById("successMsg").style.visibility = "hidden";
        }, 2000);
        setTimeout(() => {
            navigate("/appr");
        }, 2000);
    }

    return (
        <div className="d-flex justify-content-center">
            {team !== null && teammate !== null ?
                <div style={{ width: '80vw' }}>
                    <nav aria-label="breadcrumb">
                        {breadType === 'team' ?
                            <ol className="breadcrumb bg-transparent pl-0">
                                <li className="breadcrumb-item"><Link to="/home">Home</Link></li>
                                <li className="breadcrumb-item"><Link to="/teams">My Teams</Link></li>
                                <li className="breadcrumb-item"><Link to="/myteam" state={{ teamid: team.id }}>{team.name}</Link></li>
                                <li className="breadcrumb-item" ><Link to="/teammate" state={{ teamid: team.id, teammate: teammate }}>{teammate.credentials.name}</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Send Appreciation</li>
                            </ol>
                            : <ol className="breadcrumb bg-transparent pl-0">
                                <li className="breadcrumb-item"><Link to="/home">Home</Link></li>
                                <li className="breadcrumb-item"><Link to="/appr">Give Appreciation</Link></li>
                                <li className="breadcrumb-item" >{teammate.credentials.name}</li>
                                <li className="breadcrumb-item active" aria-current="page">Send Appreciation</li>
                            </ol>}
                    </nav>
                    <div>
                        <div>
                            <div className="d-flex headerUnderline justify-content-start">
                                <h4 className="mb-0 mt-4">Send Appreciation</h4>
                            </div>
                        </div>
                        <form onSubmit={appreciate}>
                            <h5 className="mt-4 mb-1 text-left">Select Badge</h5>
                            <hr />
                            {/* Favourites */}
                            <h6 className="text-left text-secondary"><b>Favourites</b></h6>
                            <hr />
                            <div className="form-group d-flex justify-content-around mx-5 mb-4 mt-2" data-toggle="buttons">
                                <div className="form-check form-check-inline">
                                    <label className="form-check-label" for="favs1">
                                        <input className="form-check-input radio" type="radio" name="badge" id="favs1" value="thanks" />
                                        <img src={warm} alt="thanks" width='120px' height='120px' />
                                    </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <label className="form-check-label" for="favs2">
                                        <input className="form-check-input radio" type="radio" name="badge" id="favs2" value="diversity" />
                                        <img src={teamPlayer} alt="diversity" width='120px' height='120px' />
                                    </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <label className="form-check-label" for="favs3">
                                        <input className="form-check-input radio" type="radio" name="badge" id="favs3" value="inspiring" />
                                        <img src={supportive} alt="inspiring" width='120px' height='120px' />
                                    </label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <label className="form-check-label" for="favs4">
                                        <input className="form-check-input radio" type="radio" name="badge" id="favs4" value="teamPlayer" />
                                        <img src={thanks} alt="teamPlayer" width='120px' height='120px' />
                                    </label>
                                </div>
                            </div>

                            {/* All */}
                            <h6 className="text-left text-secondary mt-4"><b>All</b></h6>
                            <hr />
                            <div className="form-group" data-toggle="buttons">
                                <div className="d-flex justify-content-around mx-5 my-3">
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all1">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all1" value="thanks" />
                                            <img src={thanks} alt="thanks" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all2">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all2" value="diversity" />
                                            <img src={diversity} alt="diversity" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all3">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all3" value="inspiring" />
                                            <img src={inspiring} alt="inspiring" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all4">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all4" value="teamPlayer" />
                                            <img src={teamPlayer} alt="teamPlayer" width='120px' height='120px' />
                                        </label>
                                    </div>
                                </div>
                                <div className="d-flex justify-content-around mx-5 my-3">
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all5">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all5" value="warm" />
                                            <img src={warm} alt="warm" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all6">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all6" value="supportive" />
                                            <img src={supportive} alt="supportive" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all7">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all7" value="perspective" />
                                            <img src={perspective} alt="perspective" width='120px' height='120px' />
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label" for="all8">
                                            <input className="form-check-input radio" type="radio" name="badge" id="all8" value="beyond" />
                                            <img src={beyond} alt="beyond" width='120px' height='120px' />
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group text-left text-dark mt-5">
                                <label for="note"><b>Leave a note</b></label>
                                <textarea className="form-control" name="comment" id="note" rows="3" placeholder={"Give " + fname + " some feedback."}></textarea>
                            </div>
                            <div className="text-right">
                                <button type="submit" className="btn btn-secondary mt-3 px-5">
                                    Submit
                            </button>
                            </div>
                            <h5 className="text-middle text-success mb-5" id="successMsg" style={{ visibility: "hidden" }}>
                                Thank you for giving feedback!
                            </h5>
                        </form>
                    </div>
                </div>
                : <div></div>}
        </div>
    );
}

export default TeamMember;
