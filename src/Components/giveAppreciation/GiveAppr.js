import React, { Component } from "react";
import axios from "axios";
import { Link, navigate } from "@reach/router";
import ProfilePhoto from "../../images/SmallProfilePlaceholder.svg";
import Search from "./Search";
require('dotenv').config();

class GiveAppr extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamArray: [],
      team: null,
      teamMemberList: null,
      userName: null,
    };
    this.sendGetRequest = this.sendGetRequest.bind(this);
    this.sendGetRequestTeam = this.sendGetRequestTeam.bind(this);
    this.getMemberInfo = this.getMemberInfo.bind(this);
    this.teamList = [];
    this.teamMembersEmails = [];
    this.teamMembers = [];
  }

  /**
   * get user data
   * @param {string} userEmail
   * user email used as a reference to get data
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;
      //Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          teamArray: userData[0].profile.teams, //works
          userName: userData[0].profile.name,
        });
      }
      this.sendGetRequestTeam(this.state.teamArray[0]);
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   *
   * get team by id and add to list
   * @param {Number} teamID
   * team id number
   */
  sendGetRequestTeam = async (teamID) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/teams/${teamID}`,
        {}
      );
      const teamData = resp.data;

      // Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          team: teamData[0],
        });

        this.teamList.push(teamData);
        for (let i = 0; i < teamData[0].members.length; i++) {
          this.teamMembersEmails.push(teamData[0].members[i]);
        }
      }
      this.getMemberInfo();
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get user data using emails and add to list
   */
  getMemberInfo() {
    let ue = localStorage.getItem("user-email");
    let list = [];
    for (let i = 0; i < this.teamMembersEmails.length; i++) {
      let userEmail = this.teamMembersEmails[i];
      try {
        const resp = axios.get(
          `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
          {}
        );
        resp.then((resp) => {
          console.log("resp-data: " + resp);
          const userData = resp;
          if (
            resp.data.length === 1 &&
            userData.data[0].credentials.email !== ue
          ) {
            this.teamMembers.push(userData.data[0]);
            this.setState({
              teamMemberList: this.teamMembers,
            });
            list.push(userData);
          }
        });
      } catch (err) {
        // Handle Error Here
        console.error(err);
      }
    }
  }

  /**
   * gets logged-in user's email address and passes to sendGetRequest
   */
  componentDidMount() {
    const userEmail = localStorage.getItem("user-email");
    this.sendGetRequest(userEmail);
  }

  render() {
    const tm = this.teamMembers;
    return (
      <div className="d-flex justify-content-center">
        <div style={{ width: "80vw" }}>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-transparent pl-0">
              <li className="breadcrumb-item">
                <Link to="/home">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Give Appreciation
              </li>
            </ol>
          </nav>
          <div>
            <div className="d-flex headerUnderline justify-content-start">
              <h4 className="mt-4">Give Appreciation</h4>
            </div>
            <div className="d-flex justify-content-start">
              <h5 className="mt-4 mb-1">Team Members</h5>
            </div>
            <hr />
            {tm !== null ? (
              <div className="card-deck mt-1">
                {tm.map((item) => (
                  <div key={item.key} style={{ width: "13em" }}>
                    <div>
                      <Link
                        className="card bg-light mx-3 my-3"
                        to="/sendappr"
                        state={{
                          teammate: item,
                          team: item.profile.teams[0],
                          giveOrTeam: "give",
                        }}
                      >
                        {this.state.team !== null ? (
                          <div className="card-body" style={{ height: "16em" }}>
                            <img
                              className="card-img-top"
                              src={ProfilePhoto}
                              alt="profile-image"
                            />
                            <div className="">
                              <h6 className="card-title mt-2">
                                {item.credentials.name}
                              </h6>
                              <h6 className="card-text mt-2">
                                {item.credentials.title}
                              </h6>
                            </div>
                          </div>
                        ) : (
                          <p>this.state.team is null</p>
                        )}
                      </Link>
                    </div>
                  </div>
                ))}{" "}
              </div>
            ) : (
              <div></div>
            )}

            <div className="d-flex flex-column justify-content-start mt-4">
              <h5 className="d-flex mt-4 mb-0">
                <b>Not who you were looking for?</b>
              </h5>
              <Search />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default GiveAppr;
