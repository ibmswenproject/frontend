import { useState } from "react";
import "bootstrap/dist/css/bootstrap.css";

function Search() {
  const [allItems, setAllItems] = useState([]);
  const [displayItems, setDisplayItems] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");

  function handleChange(e) {
    const value = e.target.value;
    setSearchQuery(value);
  }

  function resetQuery() {
    setSearchQuery("");
    setDisplayItems(allItems);
  }

  return (
    <div style={{ width: "50%" }}>
      <div className="pl-0 input-group input-group-lg card-body text-center">
        <div style={{ width: "100%" }}>
          <input
            type="text"
            name="searchQuery"
            value={searchQuery}
            placeholder="Search"
            className="form-control"
            onChange={(e) => handleChange(e)}
          ></input>
        </div>
      </div>
    </div>
  );
}

export default Search;
