import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "@reach/router";
import axios from "axios";
import Badges from "../myAppreciations/Badges";
import { FaPhoneAlt, FaSlack, FaThumbsUp } from "react-icons/fa";
import { FiMail, FiClock } from "react-icons/fi";
import { BiWorld, BiEdit } from "react-icons/bi";
import { ImCross } from "react-icons/im";
import { PhoneToast } from "../profile/PhoneToast";
require('dotenv').config();

function TeamMember(props) {
  const [team, setTeam] = useState(null);
  const [teammate, setTeammate] = useState(null);
  const [firstName, setFirstName] = useState(null);
  const [appArray, setAppArray] = useState([]);

  const [phone, setPhone] = useState(false);

  function displayPhone() {
    setPhone(true);
  }

  /**
   *
   * Get team by id
   * @param {Number} teamID
   * team id number
   */
  const sendGetRequestTeam = async (teamID) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/teams/${teamID}`,
        {}
      );
      const teamData = resp.data;

      // Await until we get the response
      if (resp.data.length === 1) {
        setTeam(teamData[0]);
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * Get TeamID and Teammate props from Team page
   */
  useEffect(() => {
    const teamID = props.location.state.teamid;
    sendGetRequestTeam(teamID);

    const teammate = props.location.state.teammate;
    setTeammate(teammate);

    const splitName = teammate.credentials.name.split(" ");
    setFirstName(splitName[0]);

    const userEmail = localStorage.getItem("user-email");
    sendGetRequest(userEmail);

    console.log(teammate.credentials.email);
  }, []);

  /**
   * get appreciation array using user's email
   * @param {*} userEmail
   * user email
   */
  const sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;

      //Await until we get the response
      if (resp.data.length === 1) {
        setAppArray(userData[0].app);
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  return (
    <div>
      {team !== null && teammate !== null ? (
        <Container style={{ width: "80vw" }}>
          {phone && (
            <PhoneToast
              phoneNumber={teammate.credentials.phone}
              toggle={setPhone}
            />
          )}
          <Row>
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb bg-transparent pl-0">
                <li className="breadcrumb-item">
                  <Link to="/home">Home</Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to="/teams">My Teams</Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to="/myteam" state={{ teamid: team.id }}>
                    {team.name}
                  </Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {teammate.credentials.name}
                </li>
              </ol>
            </nav>
          </Row>
          {teammate !== null ? (
            <div>
              <Row className="headerUnderline d-flex px-0">
                <Col className="col-8">
                  <Row>
                    <h3>{teammate.credentials.name}</h3>
                    <h3 className="preferredPronouns">
                      &emsp;({teammate.credentials.pronouns})
                    </h3>
                  </Row>
                </Col>
                <Link
                  className="d-flex px-0 col-4 justify-content-end"
                  style={{ textDecoration: "none", color: "black" }}
                  to="/sendappr"
                  state={{
                    teammate: teammate,
                    team: team,
                    giveOrTeam: "team",
                  }}
                >
                  <FaThumbsUp size={25} style={{ marginRight: ".5rem" }} />
                  <h4 className="editProfile">Give {firstName} Appreciation</h4>
                </Link>
              </Row>
              <Row>
                <h3 className="jobTitle mt-2">{teammate.credentials.title}</h3>
              </Row>
              <Row className="d-flex">
                <Col>
                  <Row className="my-4">
                    <img
                      style={{ maxHeight: 350 }}
                      src={"http://"+process.env.SERVER_IP+"/" + teammate.profile.photo}
                      className="img-fluid"
                      alt="profile photo"
                    ></img>
                  </Row>
                  <Row className="d-flex">
                    <Col className="d-flex flex-{grow|shrink}-0 contactUnderline mb-2 px-0">
                      <FaPhoneAlt
                        onClick={displayPhone}
                        size={25}
                        style={{ fill: "black", marginRight: ".5rem" }}
                      />
                      <h5>
                        <a className="contactOptions" onClick={displayPhone}>
                          Call
                        </a>
                      </h5>
                    </Col>
                    <Col className="d-flex flex-{grow|shrink}-0 contactUnderline ml-3 mb-2 px-0">
                      <FiMail size={25} style={{ marginRight: ".5rem" }} />
                      <h5>
                        <a
                          className="contactOptions"
                          href={"mailto:" + teammate.credentials.email}
                        >
                          Email
                        </a>
                      </h5>
                    </Col>
                    <Col className="d-flex flex-{grow|shrink}-0 contactUnderline ml-3 mb-2 px-0">
                      <FaSlack
                        size={25}
                        style={{ fill: "black", marginRight: ".5rem" }}
                      />
                      <h5>
                        <a className="contactOptions" href={"slack://open"}>
                          Slack
                        </a>
                      </h5>
                    </Col>
                  </Row>
                  <Row>
                    <p className="d-flex">
                      Please contact me via:&nbsp;
                      <div className="contactPreferences">Slack, call</div>
                    </p>
                  </Row>
                  <Row className="mt-4">
                    <BiWorld size={30} style={{ marginRight: ".5rem" }} />
                    <p>San Francisco, CA, USA</p>
                  </Row>
                  <Row className="mb-4">
                    <FiClock size={30} style={{ marginRight: ".5rem" }} />
                    <p>9:07pm, Tue 26 Jan (PST)</p>
                  </Row>
                  <Row className="mb-2">
                    <h5>Current Availability:</h5>
                  </Row>
                  <Row>
                    <ImCross size={30} style={{ marginRight: ".5rem" }} />
                    <p>9:07pm, Tue 26 Jan (PST)</p>
                  </Row>
                </Col>
                <Col className="ml-5 col-8">
                  <Row className="headerUnderline">
                    <h3 style={{ marginBottom: ".5rem" }}>About {firstName}</h3>
                  </Row>

                  <Row>
                    <Col>
                      <Row className="description mt-2 mb-4">
                        {teammate.profile.about}
                      </Row>
                      <Row className="">
                        <h4>Interests and Hobbies</h4>
                      </Row>
                      <Row>
                        <p className="description  mb-4">None</p>
                      </Row>
                      <Row className="">
                        <h4>Pets</h4>
                      </Row>
                      <Row>
                        <p className="description  mb-4">None</p>
                      </Row>
                      <Row className="headerUnderline">
                        <h4>{firstName}'s Badges</h4>
                      </Row>
                      <Badges email={teammate.credentials.email} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          ) : (
              <div></div>
            )}
        </Container>
      ) : (
          <div></div>
        )}
    </div>
  );
}

export default TeamMember;
