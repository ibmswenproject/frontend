import React, { Component, useState } from "react";
import axios from "axios";
import { Link, navigate } from "@reach/router";
import TeamPhoto from "../../images/TeamPlaceholder.svg";
require('dotenv').config();

const TeamMember = () => {
  const [teamMember, setTeamMember] = React.useState("");
  const onClick = (event) => {
    localStorage.setItem("teamMember", event.target.value);
    setTeamMember(event.target.value);
    const team = localStorage.getItem("teamMember");
    console.log(team);
  };
};

class Teams extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamArray: [],
      teams: [],
      teamList: [],
    };
    this.sendGetRequest = this.sendGetRequest.bind(this);
    this.sendGetRequestTeam = this.sendGetRequestTeam.bind(this);
    this.teamList = [];
  }

  /**
   * Get user appreciation array by user email
   * @param {String} userEmail
   * user email
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;
      //Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          teamArray: userData[0].profile.teams, //works
        });
      }
      this.sendGetRequestTeam();
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * getting teams using id number and adding to list
   * @param {Number} teamID
   * team id number
   */
  sendGetRequestTeam = async () => {
    const arr = this.state.teamArray;
    for (let i = 0; i < arr.length; i++) {
      let teamID = arr[i];
      try {
        const resp = await axios.get(
          `http://${process.env.SERVER_IP}/api/teams/${teamID}`,
          {}
        );
        const teamData = resp.data;

        // Await until we get the response
        if (resp.data.length === 1) {
          this.teamList.push(teamData[0]);
        }
      } catch (err) {
        // Handle Error Here
        console.error(err);
      }
    }
    this.setState({
      teamList: this.teamList,
    });
  };

  /**
   * get user email from localStorage
   */
  componentDidMount() {
    const userEmail = localStorage.getItem("user-email");
    this.sendGetRequest(userEmail);
  }

  render() {
    const ta = this.teamList;

    const active = [];
    const past = [];
    for (let i = 0; i < ta.length; i++) {
      if (ta[i].status === true) {
        active.push(ta[i]);
      } else {
        past.push(ta[i]);
      }
    }

    return (
      <div className="d-flex justify-content-center">
        <div style={{ width: "80vw" }}>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-transparent pl-0">
              <li className="breadcrumb-item">
                <Link to="/home">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                My Teams
              </li>
            </ol>
          </nav>
          <div>
            <div className="d-flex headerUnderline justify-content-start">
              <h4 className="mt-4">My Teams</h4>
            </div>
            <div className="d-flex justify-content-start">
              <h6 className="mt-4 mb-1">Active Teams</h6>
            </div>
            <hr />
            {active !== [] ? (
              <div className="card-deck mt-1">
                {active.map((item) => (
                  <div key={item.key} style={{ width: "13em" }}>
                    <div>
                      <Link
                        className="card bg-light mx-3 my-3"
                        to="/myteam"
                        state={{
                          teamid: item.id,
                        }}
                      >
                        <div
                          className="card-body"
                          style={{ height: "16em" }}
                        >
                          <img
                            className="card-img-top "
                            src={TeamPhoto}
                            alt="team-photo"
                          />
                          <h5 className="card-title mt-2">{item.name}</h5>
                        </div>
                      </Link>
                    </div>
                  </div>
                ))}{" "}
              </div>
            ) : (
              <div></div>
            )}

            {past.length !== 0 ? (
              <div className="d-flex justify-content-start">
                <h6 className="mt-4 mb-1">Past Teams</h6>
                <hr />
              </div>
            ) : (
              <div></div>
            )}

            {past.length !== 0 ? (
              <div className="card-deck mt-1">
                {past.map((item) => (
                  <div key={item.key} style={{ width: "13em" }}>
                    <div>
                      <Link
                        className="card bg-light mx-3 my-3"
                        to="/appreciation"
                      >
                        <div className="card-body" style={{ height: "16em" }}>
                          <img
                            className="card-img-top "
                            src={TeamPhoto}
                            alt="team-photo"
                          />
                          <h5 className="card-title mt-2">{item.name}</h5>
                        </div>
                      </Link>
                    </div>
                  </div>
                ))}{" "}
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Teams;
