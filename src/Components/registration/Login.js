import React, { Component } from "react";
import { Link, navigate } from "@reach/router";
import axios from "axios";
import Logo from "../../images/ThumbsUp_GreyCircle.svg";
require('dotenv').config();

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * checking if user exists in database by email, if present, add them to localStorage
   * @param {*} userEmail
   * user email
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        'http://${process.env.SERVER_IP}/api/users/${userEmail}',
        {}
      );
      //Await until we get the response
      if (resp.data.length === 1) {
        localStorage.setItem("logged-user", resp.data);
        localStorage.setItem("user-email", resp.data[0].credentials.email);
        navigate("/home");
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get email from input and pass to sendGetRequest
   * @param {Event} event
   * user clicks submit
   *
   */
  handleSubmit(event) {
    event.preventDefault();
    console.log("submitted");
    let userEmail = event.target.inputEmail.value;
    this.sendGetRequest(userEmail);
  }

  /**
   * Reload page for Navigation bar
   */
  reloadOnClick() {
    setTimeout(() => {
      window.location.reload();
    }, 300);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} className="form-signin">
          <div>
            <img className="mb-4 mt-4" src={Logo} width="75" height="75" />
            <h2>Please Sign In</h2>
            <div className="d-flex justify-content-center">
              <div>
                <div>
                  <label htmlFor="inputEmail" className="sr-only">
                    Email address
                  </label>
                  <input
                    style={{ width: 300 }}
                    type="email"
                    id="inputEmail"
                    className="form-control mb-3"
                    placeholder="Email Address"
                    required
                    autofocus
                  />
                  <label htmlFor="inputPassword" className="sr-only">
                    Password
                  </label>
                  <input
                    style={{ width: 300 }}
                    type="password"
                    id="inputPassword"
                    className="form-control mb-3"
                    placeholder="Password"
                  />
                </div>
                <div className="mb-3">
                  <button
                    onClick={this.reloadOnClick}
                    style={{ width: 300 }}
                    className="btn btn-lg btn-primary btn-block mb-2"
                    type="submit"
                  >
                    Sign In
                  </button>
                  <Link to="/register">
                    <button
                      style={{ width: 300 }}
                      className="btn btn-lg btn-secondary btn-block"
                      type="submit"
                    >
                      Register
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
