import React, { useState, useRef } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import { Container, Row, Col } from "react-bootstrap";
require('dotenv').config();

const FormData = require("form-data");
class UserForm extends React.Component {
  constructor() {
    super();
    this.state = {
      photo: null,
      title: null,
      about: null,
      firstname: null,
      email: null,
      pronouns: null,
      phone: null,
    };
    this.submitUser = this.submitUser.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.formData = new FormData();
  }

  formData = new FormData();

  handleInputChange(event) {
    this.formData.append(event.target.name, event.target.value);
  }

  submitUser(event) {
    event.preventDefault();
    axios
      .post("http://"+process.env.SERVER_IP+"/api/users", this.formData, {
        headers: {
          "content-type":
            "multipart/form-data; boundary=${formData._boundary}`",
        },
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    return (
      <Container style={{ width: "80vw" }}>
        <h2 className="my-4">Add Employee</h2>
        <div className="input-group d-flex justify-content-center">
          <form
            onSubmit={this.submitUser}
            encType="multipart/formData"
            name="userform"
          >
            <div>
              <div className="form-row my-3">
                <div className="col pl-0">
                  <label className="d-flex justify-content-start">
                    Employee Name
                  </label>
                  <input
                    type="text"
                    placeholder="First Name"
                    name="firstname"
                    className="form-control"
                    ref="firstname"
                    onChange={this.handleInputChange}
                  ></input>
                </div>
                <div className="col pr-0">
                  <label style={{ color: "transparent" }}> . </label>
                  <input
                    type="text"
                    placeholder="Last Name"
                    className="form-control"
                    name="lastname"
                    ref="lastname"
                    onChange={this.handleInputChange}
                  ></input>
                </div>
              </div>
            </div>
            <div className="form-row my-3">
              <label>Job Category</label>
              <select
                className="form-control"
                name="jobcategory"
                onChange={this.handleInputChange}
              >
                <option>Software Engineer</option>
                <option>User-Experience Developer</option>
                <option>Business Analyst</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Job Title</label>
              <input
                type="text"
                className="form-control"
                ref="title"
                name="title"
                onChange={this.handleInputChange}
              ></input>
            </div>
            <div className="form-row my-3">
              <label>Office Region</label>
              <select
                className="form-control"
                name="region"
                onChange={this.handleInputChange}
              >
                <option>NZ</option>
                <option>AU</option>
                <option>USA</option>
                <option>DK</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Office Location</label>
              <select
                className="form-control"
                name="location"
                onChange={this.handleInputChange}
              >
                <option>Perth</option>
                <option>Wellington</option>
                <option>San Francisco</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Work Phone</label>
              <input
                type="phone"
                className="form-control"
                aria-describedby="help"
                ref="phone"
                name="phone"
                onChange={this.handleInputChange}
              ></input>
              <small id="help" className="form-text text-muted">
                Please use international prefix, e.g. (+64) 929 2929
              </small>
            </div>
            <div className="form-row my-3">
              <label>Email Address</label>
              <input
                type="text"
                className="form-control"
                ref="email"
                name="email"
                onChange={this.handleInputChange}
              ></input>
            </div>
            <div className="form-row my-3">
              <label>About you</label>
              <textarea
                className="form-control"
                rows="3"
                ref="about"
                name="about"
                onChange={this.handleInputChange}
              ></textarea>
            </div>
            <div className="form-row my-3">
              <label>Pronouns</label>
              <select
                className="form-control"
                ref="pronouns"
                name="pronouns"
                onChange={this.handleInputChange}
              >
                <option>he/him</option>
                <option>she/her</option>
                <option>they/them</option>
                <option>zie/zir</option>
                <option>sie/hir</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Upload Photo</label>
              <input
                type="file"
                className="form-control-file"
                id="file"
                accept="file"
                ref="file"
                name="photo"
                onChange={(event) => {
                  const file = event.target.files[0];
                  this.formData.append("photo", file);
                }}
              />
            </div>
            <button type="submit" className="btn btn-primary mb-5">
              Add Employee
            </button>
          </form>
        </div>
      </Container>
    );
  }
}

export default UserForm;
