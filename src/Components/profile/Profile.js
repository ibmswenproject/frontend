import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "@reach/router";
import axios from "axios";
import Badges from "../myAppreciations/Badges";
import { FaPhoneAlt, FaSlack } from "react-icons/fa";
import { FiMail, FiClock } from "react-icons/fi";
import { BiWorld, BiEdit } from "react-icons/bi";
import { ImCross } from "react-icons/im";
import { PhoneToast } from "./PhoneToast";
require('dotenv').config();

function Profile() {
  const [user, setUser] = useState(null);
  const [phone, setPhone] = useState(false);

  function displayPhone() {
    setPhone(true);
  }

  /**
   * get user data by user email
   * @param {*} userEmail
   * user's email
   */
  const sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;
      //Await until we get the response
      if (resp.data.length === 1) {
        setUser(userData[0]);
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get logged-in user's email from local storage
   */
  useEffect(() => {
    const userEmail = localStorage.getItem("user-email");
    console.log(userEmail);
    sendGetRequest(userEmail);
  }, []);

  return (
    <Container style={{ width: "80vw" }}>
      <Row>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-transparent pl-0">
            <li className="breadcrumb-item">
              <Link to="/home">Home</Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              My Profile
            </li>
          </ol>
        </nav>
      </Row>
      {user !== null ? (
        <div>
          <Row className="headerUnderline d-flex px-0">
            {phone && (
              <PhoneToast
                phoneNumber={user.credentials.phone}
                toggle={setPhone}
              />
            )}
            <Col className="col-2 px-0">
              <h3>My Profile</h3>
            </Col>
            <Col className="col-4 px-0">
              <h3 className="preferredPronouns">{user.credentials.pronouns}</h3>
            </Col>
            <Col className="d-flex px-0 col-6 justify-content-end">
              <BiEdit size={30} style={{ marginRight: ".5rem" }} />
              <Link className="nav-item nav-link ml-0 p-0" to="/editprofile">
                <h3 className="editProfile">Edit Profile</h3>
              </Link>
            </Col>
          </Row>
          <Row>
            <h3 className="jobTitle mt-2">{user.credentials.title}</h3>
          </Row>
          {/* <div>{user.credentials.name}</div> */}
          <Row className="d-flex">
            <Col>
              <Row className="my-4">
                <img
                  style={{ maxHeight: 350 }}
                  src={"http://"+process.env.SERVER_IP+"/" + user.profile.photo}
                  class="img-fluid"
                  alt="profile photo"
                ></img>
              </Row>
              <Row className="d-flex">
                <Col className="d-flex flex-{grow|shrink}-0 contactUnderline mb-2 px-0">
                  <FaPhoneAlt
                    onClick={displayPhone}
                    size={25}
                    style={{ fill: "black", marginRight: ".5rem" }}
                  />
                  <h5>
                    <a className="contactOptions" onClick={displayPhone}>
                      Call
                    </a>
                  </h5>
                </Col>
                <Col className="d-flex flex-{grow|shrink}-0 contactUnderline ml-3 mb-2 px-0">
                  <FiMail size={25} style={{ marginRight: ".5rem" }} />
                  <h5>
                    <a
                      className="contactOptions"
                      href={"mailto:" + user.credentials.email}
                    >
                      Email
                    </a>
                  </h5>
                </Col>
                <Col className="d-flex flex-{grow|shrink}-0 contactUnderline ml-3 mb-2 px-0">
                  <FaSlack
                    size={25}
                    style={{ fill: "black", marginRight: ".5rem" }}
                  />
                  <h5>
                    <a className="contactOptions" href={"slack://open"}>
                      Slack
                    </a>
                  </h5>
                </Col>
              </Row>
              <Row>
                <p className="d-flex">
                  Please contact me via:&nbsp;
                  <div className="contactPreferences">Slack, call</div>
                </p>
              </Row>
              <Row className="mt-4">
                <BiWorld size={30} style={{ marginRight: ".5rem" }} />
                <p>San Francisco, CA, USA</p>
              </Row>
              <Row className="mb-4">
                <FiClock size={30} style={{ marginRight: ".5rem" }} />
                <p>9:07pm, Tue 26 Jan (PST)</p>
              </Row>
              <Row className="mb-2">
                <h5>Current Availability:</h5>
              </Row>
              <Row>
                <ImCross size={30} style={{ marginRight: ".5rem" }} />
                <p>9:07pm, Tue 26 Jan (PST)</p>
              </Row>
            </Col>
            <Col className="ml-5 col-8">
              <Row className="headerUnderline">
                <h3 style={{ marginBottom: ".5rem" }}>About Me</h3>
              </Row>

              <Row>
                <Col>
                  <Row className="description mt-2 mb-4">
                    {user.profile.about}
                  </Row>
                  <Row className="">
                    <h4>Interests and Hobbies</h4>
                  </Row>
                  <Row>
                    <p className="description  mb-4">None</p>
                  </Row>
                  <Row className="">
                    <h4>Pets</h4>
                  </Row>
                  <Row>
                    <p className="description  mb-4">None</p>
                  </Row>
                  <Row className="headerUnderline">
                    <h4>My Badges</h4>
                  </Row>
                  <Badges email={user.credentials.email} />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      ) : (
          <div></div>
        )}
    </Container>
  );
}

export default Profile;
