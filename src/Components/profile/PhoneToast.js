import React from "react";
import { Toast } from "react-bootstrap";

export function PhoneToast({ toggle, phoneNumber }) {
  return (
    <Toast
      onClose={() => toggle(false)}
      style={{
        position: "absolute",
        bottom: 75,
        left: 10,
      }}
    >
      <Toast.Header>{phoneNumber}</Toast.Header>
    </Toast>
  );
}
