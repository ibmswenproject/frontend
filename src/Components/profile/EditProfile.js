import React, { useState, useRef } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import { Container, Row, Col } from "react-bootstrap";
require('dotenv').config();

const FormData = require("form-data");
class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      placeHolders: {
        firstname: null,
        email: null,
        photo: null,
        title: null,
        phone: null,
        about: null,
      },
    };

    this.formData = new FormData();
    this.submitUser = this.submitUser.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  formData = new FormData();

  handleInputChange(event) {
    this.formData.set(event.target.name, event.target.value);
  }

  /**
   * get appreciation array using user's email
   * @param {*} userEmail
   * user email
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;

      //Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          placeHolders: {
            firstname: userData[0].credentials.name,
            email: userData[0].credentials.email,
            photo: userData[0].profile.photo,
            title: userData[0].credentials.title,
            phone: userData[0].credentials.phone,
            about: userData[0].profile.about,
          },
        });
        this.formData.append("firstname", userData[0].credentials.name);

        this.formData.append("email", userData[0].credentials.email);
        this.formData.append("photo", userData[0].profile.photo);
        this.formData.append("title", userData[0].credentials.title);
        this.formData.append("phone", userData[0].credentials.phone);
        this.formData.append("about", userData[0].profile.about);
        this.formData.append("pronouns", userData[0].credentials.pronouns);
        //     this.formData.append("location", userData[0].location);
        //    this.formData.append("region", userData[0].region);
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get logged-in user email from localStorage
   */
  componentDidMount() {
    const userEmail = localStorage.getItem("user-email");
    console.log(userEmail);
    this.sendGetRequest(userEmail);
    console.log(this.formData.get("email"));
  }

  submitUser(event) {
    event.preventDefault();
    axios
      .post("http://"+process.env.SERVER_IP+"/api/edituser", this.formData, {
        headers: {
          "content-type":
            "multipart/form-data; boundary=${formData._boundary}`",
        },
      })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    return (
      <Container style={{ width: "80vw" }}>
        <h2 className="my-4">Edit Profile</h2>
        <div className="input-group d-flex justify-content-center">
          <form
            onSubmit={this.submitUser}
            encType="multipart/formData"
            name="userform"
          >
            <div>
              <div className="form-row my-3">
                <div className="col pl-0">
                  <label className="d-flex justify-content-start">
                    Employee Name
                  </label>
                  <input
                    type="text"
                    name="firstname"
                    placeholder={this.state.placeHolders.firstname}
                    className="form-control"
                    ref="firstname"
                    onChange={this.handleInputChange}
                  ></input>
                </div>
                <div className="col pr-0">
                  <label style={{ color: "transparent" }}> . </label>
                  <input
                    type="text"
                    placeholder="Last Name"
                    className="form-control"
                    name="lastname"
                    ref="lastname"
                    onChange={this.handleInputChange}
                  ></input>
                </div>
              </div>
            </div>
            <div className="form-row my-3">
              <label>Job Category</label>
              <select
                className="form-control"
                name="jobcategory"
                onChange={this.handleInputChange}
              >
                <option>Software Engineer</option>
                <option>User-Experience Developer</option>
                <option>Business Analyst</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Job Title</label>
              <input
                type="text"
                className="form-control"
                placeholder={this.state.placeHolders.title}
                ref="title"
                name="title"
                onChange={this.handleInputChange}
              ></input>
            </div>
            <div className="form-row my-3">
              <label>Office Region</label>
              <select
                className="form-control"
                name="region"
                onChange={this.handleInputChange}
              >
                <option>NZ</option>
                <option>AU</option>
                <option>USA</option>
                <option>DK</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Office Location</label>
              <select
                className="form-control"
                name="location"
                onChange={this.handleInputChange}
              >
                <option>Perth</option>
                <option>Wellington</option>
                <option>San Francisco</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Work Phone</label>
              <input
                type="phone"
                className="form-control"
                aria-describedby="help"
                ref="phone"
                name="phone"
                placeholder={this.state.placeHolders.phone}
                onChange={this.handleInputChange}
              ></input>
              <small id="help" className="form-text text-muted">
                Please use international prefix, e.g. (+64) 929 2929
              </small>
            </div>
            <div className="form-row my-3">
              <label>Email Address</label>
              <input
                type="text"
                placeholder={this.state.placeHolders.email}
                className="form-control"
                ref="email"
                name="email"
                onChange={this.handleInputChange}
              ></input>
            </div>
            <div className="form-row my-3">
              <label>About you</label>
              <textarea
                className="form-control"
                rows="3"
                placeholder={this.state.placeHolders.about}
                ref="about"
                name="about"
                onChange={this.handleInputChange}
              ></textarea>
            </div>
            <div className="form-row my-3">
              <label>Pronouns</label>
              <select
                className="form-control"
                ref="pronouns"
                name="pronouns"
                onChange={this.handleInputChange}
              >
                <option>he/him</option>
                <option>she/her</option>
                <option>they/them</option>
                <option>zie/zir</option>
                <option>sie/hir</option>
              </select>
            </div>
            <div className="form-row my-3">
              <label>Upload Photo</label>
              <input
                type="file"
                className="form-control-file"
                id="file"
                accept="file"
                ref="file"
                name="photo"
                onChange={(event) => {
                  const file = event.target.files[0];
                  this.formData.set("photo", file);
                }}
              />
            </div>
            <button type="submit" className="btn btn-primary mb-5">
              Submit Changes
            </button>
          </form>
        </div>
      </Container>
    );
  }
}

export default EditProfile;
