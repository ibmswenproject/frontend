import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Row } from "react-bootstrap";

import axios from "axios";
require('dotenv').config();

function Badges(props) {
  const [appArray, setAppArray] = useState([]);

  /**
   * Get user email
   */
  useEffect(() => {
    const { email } = props;
    console.log("prop email: " + email);
    sendGetRequest(email);
  }, []);

  /**
   * get appreciation array using user's email
   * @param {*} userEmail
   * user email
   */
  const sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;

      //Await until we get the response
      if (resp.data.length === 1) {
        setAppArray(userData[0].app);
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  return (
    <Row className="d-flex justify-content-between mt-2">
      {appArray.map((item) => (
        <img
          height={100}
          width={100}
          src={"http://"+process.env.SERVER_IP+"/" + item.badge + ".svg"}
          alt={item.badge}
          className="mx-1"
        />
      ))}
    </Row>
  );
}

export default Badges;
