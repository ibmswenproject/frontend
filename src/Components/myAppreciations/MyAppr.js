import React, { Component } from "react";
import axios from "axios";
import { Link } from "@reach/router";
import ProfilePhoto from "../../images/SmallProfilePlaceholder.svg";
require('dotenv').config();

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appArray: [],
    };
    this.senders = [];
  }

  /**
   * get appreciation array using user's email
   * @param {*} userEmail
   * user email
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://${process.env.SERVER_IP}/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;

      //Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          appArray: userData[0].app, //works
        });
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get logged-in user email from localStorage
   */
  componentDidMount() {
    const userEmail = localStorage.getItem("user-email");
    this.sendGetRequest(userEmail);
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div style={{ width: "80vw" }}>
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-transparent pl-0">
              <li className="breadcrumb-item">
                <Link to="/home">Home</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                My Appreciation
              </li>
            </ol>
          </nav>
          <div className="d-flex headerUnderline justify-content-start">
            <h4 className="mt-4">My Appreciation</h4>
          </div>
          <ul className="list-group list-group-flush">
            {this.state.appArray.map((item) => (
              <li className="list-group-item d-flex col-12 my-3" key={item.key}>
                <div className="col-8 d-flex">
                  <img
                    height={75}
                    width={75}
                    src={"{process.env.SERVER_IP}/" + item.badge + ".svg"}
                    alt={item.badge}
                    className="mx-1"
                  />
                  <div className="d-flex flex-column justify-content-between ml-3 mb-3">
                    <div className="text-left" style={{ fontSize: "1.3em" }}>
                      {item.sentence}
                    </div>
                    <div className="d-flex flex-row mt-2">{item.time}</div>
                  </div>
                </div>
                <div className="col-4 d-flex align-items-start">
                  <div className="d-flex flex-row">
                    <img
                      height={75}
                      width={75}
                      src={ProfilePhoto}
                      alt="img"
                      className="mx-4"
                    />
                    <p>{item.sender}</p>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Landing;
