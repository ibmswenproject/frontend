
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Login from '../registration/Login';
import Landing from '../home/Landing';

class Home extends Component {
    render() {
        const user = localStorage.getItem("logged-user");
        return (
            <div>
                {user === null ?
                    <div>
                        <Login />
                    </div>
                    : <div>Welcome!</div> &&
                    <div>
                        <Landing user={user} />
                    </div>
                }
            </div>
        );
    }
};

export default Home;