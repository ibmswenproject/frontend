import React, { Component } from "react";
import { Link, navigate } from "@reach/router";
import Logo from "../../images/ThumbsUp_White&Transparent.png";
import { FaSignOutAlt, FaSignInAlt, FaThinkPeaks } from "react-icons/fa";
import { HiUserAdd } from "react-icons/hi";

class Navigation extends Component {
  constructor() {
    super();
    this.state = {
      user: null,
    };
  }

  componentWillMount() {
    const loggedUser = localStorage.getItem("logged-user");
    this.setState({
      user: loggedUser,
    });
  }

  render() {
    const { logout } = this.props;
    return (
      <nav className="site-nav family-sans navbar navbar-expand bg-dark navbar-dark higher">
        <div className="container-fluid">
          <a href="#" className="navbar-left">
            <img
              style={{ maxWidth: 50 }}
              src={Logo}
              className="img-responsive img-fluid"
            />
          </a>
          <Link to="/home" className="navbar-brand">
            Our Team Tool
          </Link>
          {this.state.user === null ? (
            <div className="navbar-nav ml-auto">
              <Link className="nav-item nav-link ml-4" to="/login">
                login&ensp;
                <FaSignInAlt />
              </Link>
              <Link className="nav-item nav-link ml-4" to="/register">
                register&ensp;
                <HiUserAdd size="1.3em" />
              </Link>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <Link className="nav-item nav-link" to="/myappr">
                My Appreciation
              </Link>
              <Link className="nav-item nav-link ml-4" to="/teams">
                My Teams
              </Link>
              <Link className="nav-item nav-link ml-4" to="/appr">
                Give Appreciation
              </Link>
              <Link className="nav-item nav-link ml-4" to="/profile">
                My Profile
              </Link>
              <div
                className="nav-item nav-link ml-4"
                style={{ cursor: "pointer" }}
                onClick={logout}
              >
                Logout&ensp;
                <FaSignOutAlt />
              </div>
            </div>
          )}
        </div>
      </nav>
    );
  }
}

export default Navigation;
