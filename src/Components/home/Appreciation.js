import React, { Component } from 'react';
import axios from 'axios';
import AppreciationImage from '../../images/ThumbsUp_GreyCircle.svg';
import ProfilePhoto from '../../images/SmallProfilePlaceholder.svg'

class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appArray: [],
        }
    }

    sendGetRequest = async (userEmail) => {
        try {

            const resp = await axios.get(`http://localhost:5000/api/users/${userEmail}`, {});
            const userData = resp.data;

            //Await until we get the response
            if (resp.data.length === 1) {
                this.setState({
                    appArray: userData[0].app, 
                });
            }
        } catch (err) {
            // Handle Error Here
            console.error(err);
        }
    };

    componentDidMount() {
        const userEmail = localStorage.getItem("user-email");
        this.sendGetRequest(userEmail);
    }

    render() {
        var count = 0;
        return (
            <div style={{width: "100%"}}>
                <ul className="list-group list-group-flush" >
                    {/* map */}
                    {this.state.appArray.map((item) => (
                        <li className="list-group-item d-flex justify-content-left col-12" key={item.key}>
                            <div className="col-8 d-flex align-items-start">
                                <img height={75} width={75} src={AppreciationImage} alt="badge" className="mx-1" />
                                <div className="justify-content-center">
                                    <p>{this.state.appArray[count].sentence}</p>
                                </div>
                            </div>
                            <div className="col-4 d-flex align-items-end">
                                <img height={75} width={75} src={ProfilePhoto} alt="img" className="mx-4" />
                                <p >{this.state.appArray[count].sender}</p>
                            </div>
                            <div style={{ display: 'none' }}>{count++}</div>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default Landing;
