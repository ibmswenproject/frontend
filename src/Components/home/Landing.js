import React, { Component } from "react";
import axios from "axios";
import { Link, navigate } from "@reach/router";
import ProfilePhoto from "../../images/ProfilePlaceholder.svg";
import TeamPhoto from "../../images/TeamPlaceholder.svg";
import AppreciationPhoto from "../../images/ThumbsUp_B&W.svg";

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appArray: [],
    };
    this.sendGetRequest = this.sendGetRequest.bind(this);
  }

  /**
   * get user data with user's email
   * @param {*} userEmail
   * user email
   */
  sendGetRequest = async (userEmail) => {
    try {
      const resp = await axios.get(
        `http://localhost:5000/api/users/${userEmail}`,
        {}
      );
      const userData = resp.data;
      //Await until we get the response
      if (resp.data.length === 1) {
        this.setState({
          appArray: userData[0].app, //works
        });
      }
    } catch (err) {
      // Handle Error Here
      console.error(err);
    }
  };

  /**
   * get user email fro localStorage
   */
  componentDidMount() {
    const userEmail = localStorage.getItem("user-email");
    this.sendGetRequest(userEmail);
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div style={{ width: "80vw" }}>
          <div className="card-deck mt-5 mx-4" style={{ height: "40vh" }}>
            <div style={{ width: "25vw" }}>
              <Link className="card bg-light mx-4" to="/myappr">
                <img
                  className="card-img"
                  alt="generic-profile-image"
                  src={ProfilePhoto}
                />
                <div className="card-img-overlay">
                  <h5 className="title">My Appreciation</h5>
                </div>
              </Link>
            </div>
            <div style={{ width: "25vw" }}>
              <Link className="card bg-light mx-4" to="/teams">
                <img
                  className="card-img"
                  alt="generic-profile-image"
                  src={TeamPhoto}
                />
                <div className="card-img-overlay">
                  <h5 className="title">My Teams</h5>
                </div>
              </Link>
            </div>
            <div style={{ width: "25vw" }}>
              <Link className="card bg-light mx-4" to="/appr">
                <img
                  className="card-img"
                  alt="generic-profile-image"
                  src={AppreciationPhoto}
                />
                <div className="card-img-overlay">
                  <h5 className="title">Give Appreciation</h5>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
