import { Component } from "react";
import "./App.css";

import { Router, navigate } from "@reach/router";
import Navigation from "./Components/home/Navigation";
import Home from "./Components/home/Home";
import Register from "./Components/registration/Register";
import Login from "./Components/registration/Login";
import Landing from "./Components/home/Landing";
import Profile from "./Components/profile/Profile";
import Teams from "./Components/teams/Teams";
import GiveAppr from './Components/giveAppreciation/GiveAppr';
import MyAppr from './Components/myAppreciations/MyAppr';
import SendAppr from './Components/giveAppreciation/SendAppr';
import TeamPage from './Components/teams/TeamPage';
import TeamMember from "./Components/teams/TeamMember";
import EditProfile from './Components/profile/EditProfile';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      seen: false,
    };
    //binding this to functions
    this.logout = this.logout.bind(this);
    //setting global variable
    this.user = localStorage.getItem("logged-user");
  }

  /**
   * logout
   */
  logout() {
    this.setState({
      user: null,
    });
    localStorage.clear();
    navigate(`/home`);
    window.location.reload();
  }

  render() {
    return (
      <div className="App">
        <Navigation logout={this.logout} />
        <Router>
          <Home path="/">
            <Login path="/login" />
            <Landing path="/landing" />
          </Home>
          <Profile path="/profile" />
          <EditProfile path = "/editprofile" />
          <Teams path="/teams" />
          <TeamPage path="myteam" />
          <TeamMember path="teammate" />
          <Register path="/register" />
          <GiveAppr path="/appr" />
          <MyAppr path="/myappr" />
          <SendAppr path="sendappr" />
        </Router>
      </div>
    );
  }
}

export default App;
